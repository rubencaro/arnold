package processor

import (
	"container/list"
	"fmt"
	"strings"
	"time"

	"go.i3wm.org/i3/v4"

	"gitlab.com/rubencaro/arnold/log"
)

// Params is the struct holding the state for the switcher.Switcher
type Params struct {
	// live is the ordered list of windows being visited
	live *list.List

	// snap is initially a snapshot of live, and it's the ordered list of windows being browsed
	snap *list.List

	// snapExpiration is the time where the snap list should be recreated from live
	snapExpiration time.Duration

	// SnapExpirationStep is the amount of time the snapExpiration is increased
	// every time there's some user interaction
	SnapExpirationStep time.Duration

	// WindowEvents is the channel to send new WindowEvents
	WindowEvents chan *i3.WindowEvent

	// lastVisited is tracking the last visited Element
	lastVisited *list.Element

	// mockable i3.RunCommand handle
	RunCommand RunCommandFunc

	// mockable delayed function, to be run while browsing the snap list
	delayed func(params *Params, el *payload)

	// mockable focusNext function
	focusNext func(params *Params)
}

// RunCommandFunc is the func type to mock i3.RunCommand
type RunCommandFunc func(command string) ([]i3.CommandResult, error)

// Processor is the interface for the processing logic
type Processor interface {
	ProcessWindowEvent(ev *i3.WindowEvent)
	ProcessBindingEvent(ev *i3.BindingEvent)
}

// DefaultProcessor is the default implementation of Processor
type DefaultProcessor struct {
	Pars *Params
}

// ProcessWindowEvent processes given window event and keeps
// live list up to date.
// It will discard events while the list is being consulted.
func (p *DefaultProcessor) ProcessWindowEvent(ev *i3.WindowEvent) {
	// ignore events if still traversing the list
	now := time.Duration(time.Now().UnixNano())
	if now < p.Pars.snapExpiration {
		return
	}
	// push to front if not there already
	maybePushToFront(ev, p.Pars.live)
	log.SpitList("New live:", p.Pars.live)
}

// ProcessBindingEvent processes given binding event
func (p *DefaultProcessor) ProcessBindingEvent(ev *i3.BindingEvent) {
	cmd := ev.Binding.Command
	log.Spit("hey:", cmd)
	switch {
	case cmd == "nop focusNext":
		p.Pars.focusNext(p.Pars)
	case strings.HasPrefix(cmd, "nop toggleLogger"):
		log.Toggle()
	default:
		log.Spit("Unknown cmd: %v", cmd)
	}
}

// NewDefaultProcessor will create a new DefaultProcessor struct
func NewDefaultProcessor(params *Params) *DefaultProcessor {
	params.RunCommand = i3.RunCommand
	params.live = list.New()
	params.delayed = func(p *Params, v *payload) { go delayedMaybeSaveValue(p, v, nil) }
	params.focusNext = focusNext
	return &DefaultProcessor{Pars: params}
}

// focusNext will change focus to the next entry in the snapshot list.
// It will refresh the snapshot if needed.
func focusNext(params *Params) {
	refreshSnap(params)
	tryFocusNextInSnap(params, 3)
}

// refreshSnap will update the snap list if it expired.
// It will always update the snapExpiration value so that the user
// has time to keep wandering through the list after each interaction.
func refreshSnap(params *Params) {
	now := time.Duration(time.Now().UnixNano())
	if now > params.snapExpiration {
		log.Spit("Refreshing snapshot")
		params.snap = list.New()
		params.snap.PushFrontList(params.live)
		params.lastVisited = params.snap.Front()
		log.SpitList("New snap:", params.snap)
	}
	params.snapExpiration = now + params.SnapExpirationStep
}

// tryFocusNextInSnap will recur itself triesLeft times
// until it is able to focus the next element in the snap list
func tryFocusNextInSnap(params *Params, triesLeft int) {
	if triesLeft == 0 {
		return
	}
	el := getNextValue(params)
	if el == nil {
		return
	}
	_, err := params.RunCommand(fmt.Sprintf(`[con_id="%d"] focus`, el.ID))
	if err != nil {
		log.Spit("Error: %v", err)
		removeWindow(params, el)
		tryFocusNextInSnap(params, triesLeft-1)
		return
	}
	log.Spit("Focus changed to %v", el)
	// push it to the list if it's finally the selected one
	// but only after some delay
	params.delayed(params, el)
}

// getNextValue will return the next value in the snap list,
// the first one if previous was the last, or nil if it there are no elements.
// It honors and updates the lastVisited reference in given params.
func getNextValue(params *Params) *payload {
	updateLastVisited(params)
	if params.lastVisited == nil {
		return nil
	}
	res, ok := params.lastVisited.Value.(*payload)
	if !ok {
		log.Spit("lastVisited had a value that was not a `payload`! Ignoring it: %v", params.lastVisited.Value)
		return nil
	}
	return res
}

// updateLastVisited will change lastVisited Value to be next in the snap list.
// That will be the Front element of snap if there is no current lastVisited or no next.
func updateLastVisited(params *Params) {
	if params.lastVisited == nil {
		params.lastVisited = params.snap.Front()
		return
	}
	next := params.lastVisited.Next()
	if next == nil {
		next = params.snap.Front()
	}
	params.lastVisited = next
}

// removeWindow will remove references to window referred by given payload.
// It's usually because of the window not existing anymore.
// It'll be added to the list eventually if the problem is i3 server.
func removeWindow(params *Params, el *payload) {
	removeID(params.live, el)
	removeID(params.snap, el)
	updateLastVisited(params)
}

// removeID removes any previous occurrence of ID in given payload
func removeID(l *list.List, v *payload) {
	for e := l.Front(); e != nil; e = e.Next() {
		val, ok := e.Value.(*payload)
		if !ok {
			log.Spit("Found a value in given list that was not a `payload`! Will be removed: %v", e.Value)
		}
		if !ok || val.ID == v.ID {
			log.Spit("Removing: %v", v)
			l.Remove(e)
			removeID(l, v)
		}
	}
}

// delayedMaybeSaveValue will wait for snapExpirationStep + 1 ms
// and if by then snap is already expired, then it saves the
// given value on the top of the list.
func delayedMaybeSaveValue(params *Params, el *payload, done chan<- bool) {
	// wait
	time.Sleep(params.SnapExpirationStep + time.Millisecond)

	// if it's already expired then it must be saved
	now := time.Duration(time.Now().UnixNano())
	log.Spit("Delayed: now %v expiration %v", now, params.snapExpiration)
	if now > params.snapExpiration {
		// recreate the event for given value just like the processor needs it
		// TODO: do this in a separate struct to translate inner type from outer one,
		//   with constructors like NewWindowEventFromI3Event and NewWindowEventFromData
		ev := i3.WindowEvent{
			Container: i3.Node{
				ID:   el.ID,
				Name: el.Name,
			},
		}
		log.Spit("Saving last selection %v", ev)
		params.WindowEvents <- &ev
	}
	if done != nil {
		done <- true
	}
}

// maybePushToFront will create a payload for given WindowEvent
// and push it to the front of live list, if it's not already there
func maybePushToFront(ev *i3.WindowEvent, l *list.List) {
	v := newPayload(ev)
	log.SpitList("Current live:", l)
	if l.Front() == nil || l.Front().Value != v {
		removeID(l, v)
		log.Spit("Pushing: %v", v)
		l.PushFront(v)
	}
}

// payload is the struct holding the actual window data
// It is collected directly from the server's output. See i3.Node docs.
type payload struct {
	ID   i3.NodeID
	Name string
}

func newPayload(ev *i3.WindowEvent) *payload {
	return &payload{ID: ev.Container.ID, Name: ev.Container.Name}
}
