package processor

import (
	"container/list"
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	procMocks "gitlab.com/rubencaro/arnold/mocks/processor"
	"go.i3wm.org/i3/v4"
)

func TestMaybePushToFront(t *testing.T) {
	ev := &i3.WindowEvent{Container: i3.Node{ID: 1, Name: "stub event"}}
	ev2 := &i3.WindowEvent{Container: i3.Node{ID: 2, Name: "stub event2"}}

	// empty list -> ev gets pushed to front
	l := list.New()
	maybePushToFront(ev, l)
	require.Equal(t, 1, l.Len())
	require.Equal(t, ev.Container.Name, getNames(t, l)[0])

	// ev is already in front -> nothing happens
	l = newEventList(ev, ev2)
	maybePushToFront(ev, l)
	require.Equal(t, 2, l.Len())
	require.Equal(t, ev.Container.Name, getNames(t, l)[0])
	require.Equal(t, ev2.Container.Name, getNames(t, l)[1])

	// ev is there, but not in front -> it gets pushed to front and removed from elsewhere
	l = newEventList(ev2, ev)
	maybePushToFront(ev, l)
	require.Equal(t, 2, l.Len())
	require.Equal(t, ev.Container.Name, getNames(t, l)[0])
	require.Equal(t, ev2.Container.Name, getNames(t, l)[1])
}

func TestRemoveID(t *testing.T) {
	ev := &i3.WindowEvent{Container: i3.Node{ID: 1, Name: "stub event"}}
	ev2 := &i3.WindowEvent{Container: i3.Node{ID: 2, Name: "stub event2"}}
	ev3 := &i3.WindowEvent{Container: i3.Node{ID: 2, Name: "stub event3"}}

	tests := []struct {
		name     string
		l        *list.List
		v        *payload
		expected *list.List
	}{
		{"works for empty list    ", list.New(), newPayload(ev), list.New()},
		{"does nothing if no match", newEventList(ev2, ev3), newPayload(ev), newEventList(ev2, ev3)},
		{"removes if first        ", newEventList(ev, ev2, ev3), newPayload(ev), newEventList(ev2, ev3)},
		{"removes if last         ", newEventList(ev2, ev3, ev), newPayload(ev), newEventList(ev2, ev3)},
		{"removes if in the middle", newEventList(ev2, ev, ev3), newPayload(ev), newEventList(ev2, ev3)},
		{"removes multiple        ", newEventList(ev, ev2, ev, ev3, ev, ev), newPayload(ev), newEventList(ev2, ev3)},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			removeID(tt.l, tt.v)
			require.Equal(t, tt.expected, tt.l)
		})
	}
}

func TestRemoveWindow(t *testing.T) {
	ev := &i3.WindowEvent{Container: i3.Node{ID: 1, Name: "stub event"}}
	ev2 := &i3.WindowEvent{Container: i3.Node{ID: 2, Name: "stub event2"}}
	ev3 := &i3.WindowEvent{Container: i3.Node{ID: 3, Name: "stub event3"}}
	pars := &Params{live: newEventList(ev, ev2, ev3)}
	short := newEventList(ev2, ev3)
	refreshSnap(pars)

	removeWindow(pars, newPayload(ev))
	require.Equal(t, short, pars.live)
	require.Equal(t, short, pars.snap)
}

func TestProcessBindingEvent(t *testing.T) {
	fn := &focusNextMock{}
	fn.On("Execute", mock.Anything).Return()
	pars := &Params{}
	proc := NewDefaultProcessor(pars)
	pars.focusNext = fn.Execute
	ev := &i3.BindingEvent{}

	ev.Binding.Command = "nop focusNext"
	proc.ProcessBindingEvent(ev)
	fn.AssertCalled(t, "Execute", pars)

	// TODO: test 'Toggle' and 'Unknown' cases too
}

func TestProcessWindowEvent(t *testing.T) {
	rc := &procMocks.RunCommandFunc{}
	rc.On("Execute", mock.Anything).Return(nil, nil)

	pars := &Params{RunCommand: rc.Execute}
	proc := NewDefaultProcessor(pars)
	ev := &i3.WindowEvent{Container: i3.Node{ID: 1, Name: "stub event"}}
	ev2 := &i3.WindowEvent{Container: i3.Node{ID: 2, Name: "stub event2"}}

	// adds if empty
	proc.ProcessWindowEvent(ev)

	rc.AssertNotCalled(t, "Execute", mock.Anything)
	require.Equal(t, 1, pars.live.Len())
	v, ok := pars.live.Front().Value.(*payload)
	require.True(t, ok)
	require.Equal(t, ev.Container.Name, v.Name)

	// does not add twice
	proc.ProcessWindowEvent(ev)

	require.Equal(t, 1, pars.live.Len())

	// adds new ones to front
	proc.ProcessWindowEvent(ev2)

	require.Equal(t, 2, pars.live.Len())
	v, ok = pars.live.Front().Value.(*payload)
	require.True(t, ok)
	require.Equal(t, ev2.Container.Name, v.Name)

	// moves existing ones to front
	proc.ProcessWindowEvent(ev)

	require.Equal(t, 2, pars.live.Len())
	v, ok = pars.live.Front().Value.(*payload)
	require.True(t, ok)
	require.Equal(t, ev.Container.Name, v.Name)
	v, ok = pars.live.Back().Value.(*payload)
	require.True(t, ok)
	require.Equal(t, ev2.Container.Name, v.Name)

	// ignores while traversing
	pars.snapExpiration = now() + 5*time.Second // expire in the future
	proc.ProcessWindowEvent(ev2)

	require.Equal(t, 2, pars.live.Len())
	v, ok = pars.live.Front().Value.(*payload)
	require.True(t, ok)
	require.Equal(t, ev.Container.Name, v.Name)
	v, ok = pars.live.Back().Value.(*payload)
	require.True(t, ok)
	require.Equal(t, ev2.Container.Name, v.Name)

	rc.AssertNotCalled(t, "Execute", mock.Anything)
}
func TestRefreshSnap(t *testing.T) {
	ev := &i3.WindowEvent{Container: i3.Node{ID: 1, Name: "stub event"}}
	ev2 := &i3.WindowEvent{Container: i3.Node{ID: 2, Name: "stub event2"}}
	ev3 := &i3.WindowEvent{Container: i3.Node{ID: 3, Name: "stub event3"}}
	pars := &Params{live: newEventList(ev, ev2)}

	// snap is not expired, so it's not refreshed
	// but expiration gets updated using the snapExpirationStep
	pars.snapExpiration = now() + 5*time.Second // expire in the future
	prevNow := now()

	refreshSnap(pars)

	require.True(t, prevNow < pars.snapExpiration)
	require.True(t, pars.snapExpiration < now()+pars.SnapExpirationStep)
	require.Nil(t, pars.lastVisited)

	// snap is expired, so it's refreshed
	// and still expiration gets updated using the snapExpirationStep
	pars.snapExpiration = 0 // expired already
	prevNow = now()

	refreshSnap(pars)

	require.True(t, prevNow < pars.snapExpiration)
	require.True(t, pars.snapExpiration < now()+pars.SnapExpirationStep)
	require.Equal(t, pars.live.Front(), pars.lastVisited)
	require.Equal(t, 2, pars.snap.Len())

	// snap is expired, so it's refreshed, overwritten with the new live list
	// and still expiration gets updated using the snapExpirationStep
	pars.snapExpiration = 0 // expired already
	prevNow = now()
	pars.live = newEventList(ev3, ev, ev2)

	refreshSnap(pars)

	require.True(t, prevNow < pars.snapExpiration)
	require.True(t, pars.snapExpiration < now()+pars.SnapExpirationStep)
	require.Equal(t, pars.live.Front(), pars.lastVisited)
	require.Equal(t, 3, pars.snap.Len())
}

func TestGetNextValue(t *testing.T) {
	ev := &i3.WindowEvent{Container: i3.Node{ID: 1, Name: "stub event"}}
	ev2 := &i3.WindowEvent{Container: i3.Node{ID: 2, Name: "stub event2"}}
	ev3 := &i3.WindowEvent{Container: i3.Node{ID: 3, Name: "stub event3"}}
	pars := &Params{live: newEventList(ev, ev2, ev3)}
	refreshSnap(pars)

	// we are at 1, change to 2, next is 3
	res := getNextValue(pars)
	require.Equal(t, newPayload(ev2), res)
	require.Equal(t, newPayload(ev3), pars.lastVisited.Next().Value)

	// we are at 2, change to 3, next is nil
	res = getNextValue(pars)
	require.Equal(t, newPayload(ev3), res)
	require.Nil(t, pars.lastVisited.Next())

	// we are at 3, change to 1, next is 2
	res = getNextValue(pars)
	require.Equal(t, newPayload(ev), res)
	require.Equal(t, newPayload(ev2), pars.lastVisited.Next().Value)
}

func TestFocusNext(t *testing.T) {
	ev := &i3.WindowEvent{Container: i3.Node{ID: 1, Name: "stub event"}}
	ev2 := &i3.WindowEvent{Container: i3.Node{ID: 2, Name: "stub event2"}}
	ev3 := &i3.WindowEvent{Container: i3.Node{ID: 3, Name: "stub event3"}}
	pars := &Params{
		live: newEventList(ev, ev2, ev3),
	}
	// refreshSnap now, and then prevent from refreshing again
	refreshSnap(pars)
	pars.snapExpiration = time.Duration(time.Now().UnixNano()) + 2*time.Second
	pars.SnapExpirationStep = 2 * time.Second

	// sequence of calls to RunCommandFunc
	rc := &procMocks.RunCommandFunc{}
	rc.On("Execute", `[con_id="2"] focus`).Return(nil, nil).Once()
	rc.On("Execute", `[con_id="3"] focus`).Return(nil, nil).Once()
	rc.On("Execute", `[con_id="1"] focus`).Return(nil, nil).Once()
	rc.On("Execute", `[con_id="2"] focus`).Return(nil, nil).Once()
	pars.RunCommand = rc.Execute
	// delayed gets called every time
	dm := &delayedFuncMock{}
	dm.On("Execute", pars, mock.Anything).Return().Times(4)
	pars.delayed = dm.Execute

	// we are at 1, change to 2, next is 3
	focusNext(pars)
	// we are at 2, change to 3, next is nil
	focusNext(pars)
	// we are at 3, change to 1, next is 2
	focusNext(pars)
	// we are at 1, change to 2, next is 3
	focusNext(pars)

	mock.AssertExpectationsForObjects(t, rc, dm)
}

func TestTryFocusNextInSnap(t *testing.T) {
	ev := &i3.WindowEvent{Container: i3.Node{ID: 1, Name: "stub event"}}
	ev2 := &i3.WindowEvent{Container: i3.Node{ID: 2, Name: "stub event2"}}
	ev3 := &i3.WindowEvent{Container: i3.Node{ID: 3, Name: "stub event3"}}
	pars := &Params{
		live: newEventList(ev, ev2, ev3),
	}
	// refreshSnap now, and then prevent from refreshing again
	refreshSnap(pars)
	pars.snapExpiration = time.Duration(time.Now().UnixNano()) + 2*time.Second
	pars.SnapExpirationStep = 2 * time.Second

	// failing for 2 and 3, not for 1
	// 2 and 3 are removed from the list, 1 is on focus
	rc := &procMocks.RunCommandFunc{}
	rc.On("Execute", `[con_id="2"] focus`).Return(nil, errors.New("something failed")).Once()
	rc.On("Execute", `[con_id="3"] focus`).Return(nil, errors.New("something failed")).Once()
	rc.On("Execute", `[con_id="1"] focus`).Return(nil, nil).Once()
	pars.RunCommand = rc.Execute
	// only the last one calls the delayed func
	dm := &delayedFuncMock{}
	dm.On("Execute", pars, mock.Anything).Return().Once()
	pars.delayed = dm.Execute

	tryFocusNextInSnap(pars, 5)
	mock.AssertExpectationsForObjects(t, rc, dm)
}

func TestDelayedMaybeSaveValue(t *testing.T) {
	ev := &i3.WindowEvent{Container: i3.Node{ID: 1, Name: "stub event"}}
	weChan := make(chan *i3.WindowEvent)
	weChan2 := make(chan *i3.WindowEvent)
	done := make(chan bool)
	var we, we2 *i3.WindowEvent
	pars := &Params{
		WindowEvents: weChan,
	}
	pars2 := &Params{
		WindowEvents: weChan2,
	}

	// expired, the event built from the payload should be sent through the channel
	go delayedMaybeSaveValue(pars, newPayload(ev), done)
	require.Eventually(t, func() bool { we = <-weChan; return <-done }, 10*time.Millisecond, time.Millisecond)
	require.Equal(t, ev, we)

	// not expired, nothing happens
	pars.snapExpiration = time.Duration(time.Now().UnixNano()) + time.Hour
	go delayedMaybeSaveValue(pars, newPayload(ev), done)
	require.Never(t, func() bool { <-weChan; return true }, 100*time.Millisecond, time.Millisecond)
	require.Eventually(t, func() bool { return <-done }, 10*time.Millisecond, time.Millisecond)

	// expired again, eventually
	go delayedMaybeSaveValue(pars2, newPayload(ev), done)
	require.Eventually(t, func() bool { we2 = <-weChan2; return <-done }, 10*time.Millisecond, time.Millisecond)
	require.Equal(t, ev, we2)
}

func newEventList(events ...*i3.WindowEvent) *list.List {
	l := list.New()
	for _, e := range events {
		l.PushBack(newPayload(e))
	}
	return l
}

func now() time.Duration {
	return time.Duration(time.Now().UnixNano())
}

func getNames(t *testing.T, l *list.List) []string {
	require.NotNil(t, l)
	res := []string{}
	for e := l.Front(); e != nil; e = e.Next() {
		require.NotNil(t, e.Value)
		v, ok := e.Value.(*payload)
		require.True(t, ok)
		res = append(res, v.Name)
	}
	return res
}

// delayedFuncMock is an semi-autogenerated mock type for the Params.delayed function
type delayedFuncMock struct {
	mock.Mock
}

// Execute provides a mock function with given fields: params, el
func (m *delayedFuncMock) Execute(params *Params, el *payload) {
	m.Called(params, el)
}

// focusNextMock is an semi-autogenerated mock type for the Params.focusNext function
type focusNextMock struct {
	mock.Mock
}

// Execute provides a mock function with given fields: params, el
func (m *focusNextMock) Execute(params *Params) {
	m.Called(params)
}
