// Package switcher holds the logic for the window switcher feature.
// The main entry point is the Switcher.Run func, which is meant to be run on its own goroutine.
// It requires two channels for communication of events from i3/sway (WindowEvents and BindingEvents).
// All of them wrapped inside a Params struct.
package switcher

import (
	"time"

	"go.i3wm.org/i3/v4"

	"gitlab.com/rubencaro/arnold/log"
	proc "gitlab.com/rubencaro/arnold/processor"
)

// Params is the struct holding the state for the switcher.Switcher
type Params struct {

	// WindowEvents is the channel to send new WindowEvents
	WindowEvents chan *i3.WindowEvent

	// BindingEvents is the channel for incoming i3.BindingEvent
	BindingEvents chan *i3.BindingEvent

	// mockable Processor
	Processor proc.Processor

	// SnapExpirationStep is the amount of time the snapExpiration is increased
	// every time there's some user interaction
	SnapExpirationStep time.Duration
}

// Switcher is the main struct to work with the switcher
type Switcher struct {
	Pars *Params
}

// New will create a new Switcher struct
func New(params *Params) *Switcher {
	if params.Processor == nil {
		params.Processor = proc.NewDefaultProcessor(&proc.Params{
			WindowEvents:       params.WindowEvents,
			SnapExpirationStep: params.SnapExpirationStep,
		})
	}
	return &Switcher{Pars: params}
}

// Run processes incoming binding events from
// both WindowEvents and BindingEvents channels.
func (c *Switcher) Run(ctl chan bool) {
	proc := c.Pars.Processor
	for {
		select {
		case we := <-c.Pars.WindowEvents:
			log.Spit("Processing window event: %v", we)
			proc.ProcessWindowEvent(we)
		case be := <-c.Pars.BindingEvents:
			log.Spit("Processing binding event: %v", be)
			proc.ProcessBindingEvent(be)
		case <-ctl:
			ctl <- true
			return
		}
	}
}
