package switcher

import (
	"testing"

	"github.com/stretchr/testify/mock"
	"go.i3wm.org/i3/v4"

	procMocks "gitlab.com/rubencaro/arnold/mocks/processor"
)

func TestRun(t *testing.T) {
	windowEvents := make(chan *i3.WindowEvent)
	bindingEvents := make(chan *i3.BindingEvent)

	proc := &procMocks.Processor{}
	proc.
		On("ProcessBindingEvent", mock.Anything).Return().
		On("ProcessWindowEvent", mock.Anything).Return()

	parms := &Params{
		Processor:     proc,
		WindowEvents:  windowEvents,
		BindingEvents: bindingEvents,
	}
	switcher := New(parms)
	ctl := make(chan bool)

	go switcher.Run(ctl)

	windowEvents <- &i3.WindowEvent{}
	bindingEvents <- &i3.BindingEvent{}
	ctl <- true
	<-ctl

	mock.AssertExpectationsForObjects(t, proc)
}
