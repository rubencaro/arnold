# Arnold

This is a server that communicates with the window manager (either i3 or sway), to help with minor things.

## Development

- Install Go by one of:
  - Running `asdf install` (recommended).
  - Installing version noted in `.tool-versions` by other means.
- Run `make setup` to install other tools.

## Build

Run `make build` to get the resulting `arnold` binary.

## Test

Run `make format` to format code.
Run `make lint` to lint code.
Run `make test` to run tests.
Run `make watch` to run tests after file changes.
Run `make mocks` to regenerate mocks to all owned interfaces.
Run `make cover` to get HTML coverage report (text report is generated after running tests).

