package log

import (
	"container/list"
	"fmt"
	stdlog "log"
	"log/syslog"

	"gitlab.com/rubencaro/arnold/config"
)

// singleton is the struct of the shared instance connected to the syslog server.
type singleton struct {
	enabled bool
	writer  *syslog.Writer
}

// sing is the actual instance connected to the syslog server.
var sing *singleton

// Init creates a connection with the syslog server.
// It returns a cleanup function meant to be called when resources can be released.
// As in:
//
//	cleanup := log.Init()
//	defer cleanup()
func Init(cnf *config.LogConfig) func() {
	w, err := syslog.New(syslog.LOG_USER, cnf.Prefix)
	if err != nil {
		stdlog.Fatal(err)
	}
	sing = &singleton{
		enabled: true,
		writer:  w,
	}
	return func() { sing.writer.Close() }
}

// SpitList will Spit the given msg and then also each element of given list.
func SpitList(msg string, l *list.List) {
	Spit(msg)
	for e := l.Front(); e != nil; e = e.Next() {
		Spit("%v", e.Value)
	}
}

// Spit will send given format and values through the log channel.
// It does nothing if log is deactivated.
// It does nothing if `Init` was not called first.
func Spit(format string, v ...any) {
	if sing != nil && sing.enabled {
		_, err := fmt.Fprintf(sing.writer, format+"\n", v...)
		if err != nil {
			stdlog.Fatalf("Cannot write to log: %v", err)
		}
	}
}

// Toggle will toggle the enable/disable status of the log.
// When disabled, any call to Spit is a noop.
func Toggle() {
	sing.enabled = !sing.enabled
}
