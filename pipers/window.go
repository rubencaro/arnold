package pipers

import (
	"gitlab.com/rubencaro/arnold/log"
	"gitlab.com/rubencaro/arnold/wm"

	"go.i3wm.org/i3/v4"
)

// WindowEvents quickly consumes i3.WindowEvent and queues them on
// the events channel from given state to prevent i3
// from deadlock while buffering them
func WindowEvents(events chan<- *i3.WindowEvent, s wm.Subscriber, done chan<- bool) {
	if s == nil {
		s = &wm.I3Subscriber{}
	}

	recv := s.Subscribe(i3.WindowEventType)
	for recv.Next() {
		// safe to assume it's a i3.WindowEvent, we subscribed only to them
		ev := recv.Event().(*i3.WindowEvent)
		log.Spit("piped window event: %v", ev)
		events <- ev
	}
	log.Spit("Closing window piper: ", recv.Close())

	if done == nil {
		log.Spit("Lost connection with i3/sway. Panicking.")
		panic("Aaarrrgghhhh")
	}
	done <- true
}
