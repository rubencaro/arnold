package pipers

import (
	"testing"

	mocks "gitlab.com/rubencaro/arnold/mocks/wm"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"go.i3wm.org/i3/v4"
)

func TestBindingEvents(t *testing.T) {
	bindingEvents := make(chan *i3.BindingEvent, 100)
	done := make(chan bool)

	evt := i3.BindingEvent{}
	evtReceiver := &mocks.EventReceiver{}
	evtReceiver.
		On("Next").Return(true).Once().
		On("Event").Return(&evt).
		On("Next").Return(false).
		On("Close").Return(nil)

	subscriber := &mocks.Subscriber{}
	subscriber.On("Subscribe", mock.Anything).Return(evtReceiver)

	go BindingEvents(bindingEvents, subscriber, done)

	recv := <-bindingEvents
	assert.Equal(t, &evt, recv)

	<-done

	mock.AssertExpectationsForObjects(t, evtReceiver, subscriber)
}
