package pipers

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"go.i3wm.org/i3/v4"

	mocks "gitlab.com/rubencaro/arnold/mocks/wm"
)

func TestWindowEvents(t *testing.T) {
	windowEvents := make(chan *i3.WindowEvent, 100)
	done := make(chan bool)

	evt := i3.WindowEvent{}
	evtReceiver := &mocks.EventReceiver{}
	evtReceiver.
		On("Next").Return(true).Once().
		On("Event").Return(&evt).
		On("Next").Return(false).
		On("Close").Return(nil)

	subscriber := &mocks.Subscriber{}
	subscriber.On("Subscribe", mock.Anything).Return(evtReceiver)

	go WindowEvents(windowEvents, subscriber, done)

	recv := <-windowEvents
	assert.Equal(t, &evt, recv)

	<-done

	mock.AssertExpectationsForObjects(t, evtReceiver, subscriber)
}
