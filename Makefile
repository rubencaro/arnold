# Some Makefile convenient defaults (taken from https://tech.davis-hansson.com/p/make/)
.ONESHELL:
SHELL := bash
.SHELLFLAGS := -eu -o pipefail -c
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules
.PHONY: * 

packages = $(shell go list ./... | grep -v mocks)

setup:
	go install golang.org/x/tools/cmd/goimports@latest
	go install github.com/golangci/golangci-lint/cmd/golangci-lint@latest
	go install github.com/vektra/mockery/v2@latest
	go install github.com/radovskyb/watcher/cmd/watcher@latest
	go install github.com/loov/goda@latest
	go mod tidy
	# Make new binaries known to asdf
	asdf reshim golang

mocks:
	rm -fr mocks
	mockery --all --keeptree --inpackage

format:
	go mod tidy
	goimports -w .

lint: format
	golangci-lint run

test: lint
	go test $(packages) -race -coverprofile coverage.txt

cover: test
	go tool cover -html=coverage.txt

watch:
	watcher -cmd="make test" -list -startcmd -keepalive $(shell find . -name '*.go')

build:
	go build -o arnold .../..

install:
	ln -s $(shell pwd)/arnold /home/ruben/.local/bin/

dependencies:
	goda graph -short all | dot -Tsvg -o /tmp/deps.svg
	firefox /tmp/deps.svg &

