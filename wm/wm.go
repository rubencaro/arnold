package wm

import (
	"go.i3wm.org/i3/v4"
)

// EventReceiver is the interface needed to mock events from i3/sway
type EventReceiver interface {
	Next() bool
	Event() i3.Event
	Close() error
}

// Subscriber is the interface with i3 subscription service
type Subscriber interface {
	Subscribe(eventTypes ...i3.EventType) EventReceiver
}

// I3Subscriber is the i3 implementation of the interface
type I3Subscriber struct{}

// Subscribe is just calling i3.Subscribe
func (i *I3Subscriber) Subscribe(eventTypes ...i3.EventType) EventReceiver {
	return i3.Subscribe(eventTypes...)
}
