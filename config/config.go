package config

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"reflect"
)

const configPath = "./config/"

type AppConfig struct {
	Log *LogConfig `json:"log"`
}

type LogConfig struct {
	Prefix string `json:"prefix" env:"CONFIG_LOG_PREFIX"`
	Bogus  string `json:"bogus"`
}

func Fetch() *AppConfig {
	cnf := &AppConfig{}

	// read from config.json
	cnf.MergeFile("config.json")

	// read from config-<ENV>.json
	env, ok := os.LookupEnv("ENV")
	if !ok {
		log.Fatal("ENV was not set")
	}
	cnf.MergeFile("config-" + env + ".json")

	// read from environment
	cnf.MergeEnvironment()

	return cnf
}

func (cnf *AppConfig) MergeFile(name string) {
	path, _ := filepath.Abs(filepath.Join(configPath, name))

	bytes, err := readFile(path)
	if err != nil {
		log.Printf("File %s could not be read: %s", path, err)
	}

	err = json.Unmarshal(bytes, cnf)
	if err != nil {
		log.Printf("Contents from %s could not be unmarshalled: %s", path, err)
	}
}

func (cnf *AppConfig) MergeEnvironment() {
	walkFields(reflect.ValueOf(cnf).Elem())
}

func readFile(path string) ([]byte, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, fmt.Errorf("could not open file: %w", err)
	}
	defer file.Close()

	return io.ReadAll(file)
}

// walkFields walks given struct recursively looking for fields with an 'env' tag,
// then it calls [overwriteWithEnv] with the tag value.
func walkFields(v reflect.Value) {
	for i := 0; i < v.NumField(); i++ {
		field := v.Field(i)
		if field.Kind() == reflect.Pointer {
			field = field.Elem()
		}
		if field.Kind() == reflect.Struct {
			walkFields(field)
		} else {
			tag, ok := v.Type().Field(i).Tag.Lookup("env")
			if ok {
				overwriteWithEnv(field, tag)
			}
		}
	}
}

// overwriteWithEnv will look in the environment for a variable with given name.
// If found, it will set the value of given field to the string in the environment variable.
func overwriteWithEnv(f reflect.Value, name string) {
	env, ok := os.LookupEnv(name)
	if ok {
		f.Set(reflect.ValueOf(env))
	}
}
