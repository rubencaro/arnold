package main

import (
	"time"

	"gitlab.com/rubencaro/arnold/config"
	"gitlab.com/rubencaro/arnold/log"
	"gitlab.com/rubencaro/arnold/pipers"
	"gitlab.com/rubencaro/arnold/switcher"
	"go.i3wm.org/i3/v4"
)

func main() {
	// patchForSway()

	cnf := config.Fetch()

	// Init log service
	cleanup := log.Init(cnf.Log)
	defer cleanup()

	// Channels
	windowEvents := make(chan *i3.WindowEvent, 100)
	bindingEvents := make(chan *i3.BindingEvent, 100)

	// Pipers
	go pipers.WindowEvents(windowEvents, nil, nil)
	go pipers.BindingEvents(bindingEvents, nil, nil)

	// Window switcher
	switcher.New(&switcher.Params{
		BindingEvents:      bindingEvents,
		SnapExpirationStep: 1 * time.Second,
		WindowEvents:       windowEvents,
	}).Run(nil)
}

// patchForSway will make it work with sway, see https://github.com/i3/go-i3/pull/5
// func patchForSway() {
// 	i3.SocketPathHook = func() (string, error) {
// 		out, err := exec.Command("sway", "--get-socketpath").CombinedOutput()
// 		if err != nil {
// 			return "", fmt.Errorf("getting sway socketpath: %v (output: %s)", err, out)
// 		}
// 		return string(out), nil
// 	}

// 	i3.IsRunningHook = func() bool {
// 		out, err := exec.Command("pgrep", "-c", "sway\\$").CombinedOutput()
// 		if err != nil {
// 			log.Printf("sway running: %v (output: %s)", err, out)
// 		}
// 		return bytes.Equal(out, []byte("1"))
// 	}
// }
