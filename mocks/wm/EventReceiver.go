// Code generated by mockery v2.26.1. DO NOT EDIT.

package wm

import (
	mock "github.com/stretchr/testify/mock"
	i3 "go.i3wm.org/i3/v4"
)

// EventReceiver is an autogenerated mock type for the EventReceiver type
type EventReceiver struct {
	mock.Mock
}

// Close provides a mock function with given fields:
func (_m *EventReceiver) Close() error {
	ret := _m.Called()

	var r0 error
	if rf, ok := ret.Get(0).(func() error); ok {
		r0 = rf()
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Event provides a mock function with given fields:
func (_m *EventReceiver) Event() i3.Event {
	ret := _m.Called()

	var r0 i3.Event
	if rf, ok := ret.Get(0).(func() i3.Event); ok {
		r0 = rf()
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(i3.Event)
		}
	}

	return r0
}

// Next provides a mock function with given fields:
func (_m *EventReceiver) Next() bool {
	ret := _m.Called()

	var r0 bool
	if rf, ok := ret.Get(0).(func() bool); ok {
		r0 = rf()
	} else {
		r0 = ret.Get(0).(bool)
	}

	return r0
}

type mockConstructorTestingTNewEventReceiver interface {
	mock.TestingT
	Cleanup(func())
}

// NewEventReceiver creates a new instance of EventReceiver. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
func NewEventReceiver(t mockConstructorTestingTNewEventReceiver) *EventReceiver {
	mock := &EventReceiver{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
